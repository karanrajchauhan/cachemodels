#include <iostream>
#include <fstream>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include "pin.H"
#include <list>

/*
 * NOTE: current write policy (hard coded): wb with walloc
 * dirty bit logic does not need to be implemented because we are not simulating
 * writing/communicating with higher levels of cache/memory
 */

UINT32 logPageSize;
UINT32 logPhysicalMemSize;

//Function to obtain physical page number given a virtual page number
UINT64 getPhysicalPageNumber(UINT64 virtualPageNumber)
{
    INT32 key = (INT32) virtualPageNumber;
    key = ~key + (key << 15); // key = (key << 15) - key - 1;
    key = key ^ (key >> 12);
    key = key + (key << 2);
    key = key ^ (key >> 4);
    key = key * 2057; // key = (key + (key << 3)) + (key << 11);
    key = key ^ (key >> 16);
    return (UINT32) (key&(((UINT32)(~0))>>(32-logPhysicalMemSize)));
}

class CacheModel
{
    protected:
        UINT32   logNumRows;
        UINT32   logBlockSize;
        UINT32   associativity;
        UINT64   readReqs;
        UINT64   writeReqs;
        UINT64   readHits;
        UINT64   writeHits;
        UINT32** tag;
        bool**   validBit;

    public:
        //Constructor for a cache
        CacheModel(UINT32 logNumRowsParam, UINT32 logBlockSizeParam, UINT32 associativityParam)
        {
            logNumRows = logNumRowsParam;
            logBlockSize = logBlockSizeParam;
            associativity = associativityParam;
            readReqs = 0;
            writeReqs = 0;
            readHits = 0;
            writeHits = 0;
            tag = new UINT32*[1u<<logNumRows];
            validBit = new bool*[1u<<logNumRows];
            for(UINT32 i = 0; i < 1u<<logNumRows; i++)
            {
                tag[i] = new UINT32[associativity];
                validBit[i] = new bool[associativity];
                for(UINT32 j = 0; j < associativity; j++)
                    validBit[i][j] = false;
            }
        }

        //Call this function to update the cache state whenever data is read
        virtual void readReq(UINT32 virtualAddr) = 0;

        //Call this function to update the cache state whenever data is written
        virtual void writeReq(UINT32 virtualAddr) = 0;

        //Do not modify this function
        void dumpResults(ofstream *outfile)
        {
        	*outfile << readReqs <<","<< writeReqs <<","<< readHits <<","<< writeHits <<"\n";
        }
};

CacheModel* cachePP;
CacheModel* cacheVP;
CacheModel* cacheVV;

class LruPhysIndexPhysTagCacheModel: public CacheModel
{
private:
    UINT32 indexBitMask;
    UINT32 pageOffsetBitMask;
    std::list<UINT32> *lastAccessedQs;

public:
    LruPhysIndexPhysTagCacheModel(UINT32 logNumRowsParam, UINT32 logBlockSizeParam, UINT32 associativityParam)
        : CacheModel(logNumRowsParam, logBlockSizeParam, associativityParam)
    {
        // list of queues to keep track of which entry at each index was lru
        lastAccessedQs = new std::list<UINT32>[ 1u<<logNumRowsParam ];

        // mask to get page offset bits
        pageOffsetBitMask = 0;
        for (UINT32 i = 0; i < logPageSize; ++i)
        {
            pageOffsetBitMask |= (1u << i);
        }

        // mask to get the logNumRows bits of address to be used as index
        indexBitMask = 0;
        for (UINT32 i = 0; i < logNumRowsParam; ++i)
        {
            indexBitMask |= (1u << i);
        }

        // omit lowest logBlockSizeParam bits used as cache block offset
        indexBitMask = indexBitMask << logBlockSizeParam;
    }

    void readReq(UINT32 virtualAddr)
    {
        // increment counter
        readReqs++;

        // get ppn from vpn (highest 32-logPageSize bits)
        UINT32 physicalAddr = getPhysicalPageNumber( (virtualAddr & ~pageOffsetBitMask) >> logPageSize );

        // concatenate page offset bits to ppn to get final physical address
        physicalAddr = (physicalAddr << logPageSize) | (virtualAddr & pageOffsetBitMask);

        // get index and tag
        UINT32 currIndex = (physicalAddr & indexBitMask) >> logBlockSize;
        UINT32 currTag = physicalAddr >> (logNumRows+logBlockSize);

        // check for read hit
        BOOL didHit = false;
        for (UINT32 i = 0; i < associativity; ++i)
        {
            // check if tag matches and valid bit is set
            if (validBit[currIndex][i] && tag[currIndex][i]==currTag)
            {
                // remove current tag from its now stale position in queue (later set it as mru)
                lastAccessedQs[currIndex].remove(currTag);
                didHit = true;
                readHits++;
                break;
            }
        }

        // update cache with current entry, data fetched from upper cache/mem
        if (!didHit)
        {
            // if queue size is less than associativity, it is empty spot (cold cache)
            if (lastAccessedQs[currIndex].size() < associativity)
            {
                tag[currIndex][ lastAccessedQs[currIndex].size() ] = currTag;
                validBit[currIndex][ lastAccessedQs[currIndex].size() ] = true;
            }
            else
            {
                // check which entry was lru entry at currIndex and replace it
                for (UINT32 i = 0; i < associativity; ++i)
                {
                    if (tag[currIndex][i] == lastAccessedQs[currIndex].front())
                    {
                        lastAccessedQs[currIndex].pop_front();
                        tag[currIndex][i] = currTag;
                        validBit[currIndex][i] = true;
                        break;
                    }
                }
            }
        }

        // lru entry has been removed. add current entry to back as mru
        lastAccessedQs[currIndex].push_back(currTag);
    }

    void writeReq(UINT32 virtualAddr)
    {
        // increment counter
        writeReqs++;

        // get ppn from vpn (highest 32-logPageSize bits)
        UINT32 physicalAddr = getPhysicalPageNumber( (virtualAddr & ~pageOffsetBitMask) >> logPageSize );

        // concatenate page offset bits to ppn to get final physical address
        physicalAddr = (physicalAddr << logPageSize) | (virtualAddr & pageOffsetBitMask);

        // get index and tag
        UINT32 currIndex = (physicalAddr & indexBitMask) >> logBlockSize;
        UINT32 currTag = physicalAddr >> (logNumRows+logBlockSize);

        // check for write hit
        BOOL didHit = false;
        for (UINT32 i = 0; i < associativity; ++i)
        {
            // check if tag matches. valid bit does not matter
            if (tag[currIndex][i] == currTag)
            {
                // remove current tag from its now stale position in queue (later set it as mru)
                lastAccessedQs[currIndex].remove(currTag);
                validBit[currIndex][i] = true;
                didHit = true;
                writeHits++;
                break;
            }
        }

        // write back and write allocate
        if (!didHit)
        {
            // if queue size is less than associativity, it is empty spot (cold cache)
            if (lastAccessedQs[currIndex].size() < associativity)
            {
                tag[currIndex][ lastAccessedQs[currIndex].size() ] = currTag;
                validBit[currIndex][ lastAccessedQs[currIndex].size() ] = true;
            }
            else
            {
                // check which entry was lru entry at currIndex and replace it
                for (UINT32 i = 0; i < associativity; ++i)
                {
                    if (tag[currIndex][i] == lastAccessedQs[currIndex].front())
                    {
                        lastAccessedQs[currIndex].pop_front();
                        tag[currIndex][i] = currTag;
                        validBit[currIndex][i] = true;
                        break;
                    }
                }
            }
        }

        // lru entry has been removed. add current entry to back as mru
        lastAccessedQs[currIndex].push_back(currTag);
    }
};

class LruVirIndexPhysTagCacheModel: public CacheModel
{
private:
    UINT32 indexBitMask;
    UINT32 pageOffsetBitMask;
    std::list<UINT32> *lastAccessedQs;

public:
    LruVirIndexPhysTagCacheModel(UINT32 logNumRowsParam, UINT32 logBlockSizeParam, UINT32 associativityParam)
        : CacheModel(logNumRowsParam, logBlockSizeParam, associativityParam)
    {
        // list of queues to keep track of which entry at each index was lru
        lastAccessedQs = new std::list<UINT32>[ 1u<<logNumRowsParam ];

        // mask to get page offset bits
        pageOffsetBitMask = 0;
        for (UINT32 i = 0; i < logPageSize; ++i)
        {
            pageOffsetBitMask |= (1u << i);
        }

        // mask to get the logNumRows bits of address to be used as index
        indexBitMask = 0;
        for (UINT32 i = 0; i < logNumRowsParam; ++i)
        {
            indexBitMask |= (1u << i);
        }

        // omit lowest logBlockSizeParam bits used as cache block offset
        indexBitMask = indexBitMask << logBlockSizeParam;
    }

    void readReq(UINT32 virtualAddr)
    {
        // increment counter
        readReqs++;

        // get index from virtual address
        UINT32 currIndex = (virtualAddr & indexBitMask) >> logBlockSize;

        // get ppn to use as tag
        UINT32 currTag = getPhysicalPageNumber( (virtualAddr & ~pageOffsetBitMask) >> logPageSize );

        // check for read hit
        BOOL didHit = false;
        for (UINT32 i = 0; i < associativity; ++i)
        {
            if (validBit[currIndex][i] && tag[currIndex][i]==currTag)
            {
                // remove current tag from its now stale position in queue (later set it as mru)
                lastAccessedQs[currIndex].remove(currTag);
                didHit = true;
                readHits++;
                break;
            }
        }

        // update cache with current entry, data fetched from upper cache/mem
        if (!didHit)
        {
            // if queue size is less than associativity, it is empty spot (cold cache)
            if (lastAccessedQs[currIndex].size() < associativity)
            {
                tag[currIndex][ lastAccessedQs[currIndex].size() ] = currTag;
                validBit[currIndex][ lastAccessedQs[currIndex].size() ] = true;
            }
            else
            {
                // check which entry was lru entry at currIndex and replace it
                for (UINT32 i = 0; i < associativity; ++i)
                {
                    if (tag[currIndex][i] == lastAccessedQs[currIndex].front())
                    {
                        lastAccessedQs[currIndex].pop_front();
                        tag[currIndex][i] = currTag;
                        validBit[currIndex][i] = true;
                        break;
                    }
                }
            }
        }

        // lru entry has been removed. add current entry to back as mru
        lastAccessedQs[currIndex].push_back(currTag);
    }

    void writeReq(UINT32 virtualAddr)
    {
        // increment counter
        writeReqs++;

        // get index from virtual address
        UINT32 currIndex = (virtualAddr & indexBitMask) >> logBlockSize;

        // get ppn to use as tag
        UINT32 currTag = getPhysicalPageNumber( (virtualAddr & ~pageOffsetBitMask) >> logPageSize );

        // check for write hit
        BOOL didHit = false;
        for (UINT32 i = 0; i < associativity; ++i)
        {
            if (tag[currIndex][i] == currTag)
            {
                // remove current tag from its now stale position in queue (later set it as mru)
                lastAccessedQs[currIndex].remove(currTag);
                validBit[currIndex][i] = true;
                didHit = true;
                writeHits++;
                break;
            }
        }

        // write back and write allocate
        if (!didHit)
        {
            // if queue size is less than associativity, it is empty spot (cold cache)
            if (lastAccessedQs[currIndex].size() < associativity)
            {
                tag[currIndex][ lastAccessedQs[currIndex].size() ] = currTag;
                validBit[currIndex][ lastAccessedQs[currIndex].size() ] = true;
            }
            else
            {
                // check which entry was lru entry at currIndex and replace it
                for (UINT32 i = 0; i < associativity; ++i)
                {
                    if (tag[currIndex][i] == lastAccessedQs[currIndex].front())
                    {
                        lastAccessedQs[currIndex].pop_front();
                        tag[currIndex][i] = currTag;
                        validBit[currIndex][i] = true;
                        break;
                    }
                }
            }
        }

        // lru entry has been removed. add current entry to back as mru
        lastAccessedQs[currIndex].push_back(currTag);
    }
};

class LruVirIndexVirTagCacheModel: public CacheModel
{
private:
    UINT32 indexBitMask;
    std::list<UINT32> *lastAccessedQs;

public:
    LruVirIndexVirTagCacheModel(UINT32 logNumRowsParam, UINT32 logBlockSizeParam, UINT32 associativityParam)
        : CacheModel(logNumRowsParam, logBlockSizeParam, associativityParam)
    {
        // list of queues to keep track of which entry at each index was lru
        lastAccessedQs = new std::list<UINT32>[ 1u<<logNumRowsParam ];

        // mask to get the logNumRows bits of address to be used as index
        indexBitMask = 0;
        for (UINT32 i = 0; i < logNumRowsParam; ++i)
        {
            indexBitMask |= (1u << i);
        }
        
        // omit lowest logBlockSizeParam bits used as cache block offset
        indexBitMask = indexBitMask << logBlockSizeParam;
    }

    void readReq(UINT32 virtualAddr)
    {
        // increment counter
        readReqs++;

        // get index and tag
        UINT32 currIndex = (virtualAddr & indexBitMask) >> logBlockSize;
        UINT32 currTag = virtualAddr >> (logNumRows+logBlockSize);

        // check for read hit
        BOOL didHit = false;
        for (UINT32 i = 0; i < associativity; ++i)
        {
            if (validBit[currIndex][i] && tag[currIndex][i]==currTag)
            {
                // remove current tag from its now stale position in queue (later set it as mru)
                lastAccessedQs[currIndex].remove(currTag);
                didHit = true;
                readHits++;
                break;
            }
        }

        // update cache with current entry, data fetched from upper cache/mem
        if (!didHit)
        {
            // if queue size is less than associativity, it is empty spot (cold cache)
            if (lastAccessedQs[currIndex].size() < associativity)
            {
                tag[currIndex][ lastAccessedQs[currIndex].size() ] = currTag;
                validBit[currIndex][ lastAccessedQs[currIndex].size() ] = true;
            }
            else
            {
                // check which entry was lru entry at currIndex and replace it
                for (UINT32 i = 0; i < associativity; ++i)
                {
                    if (tag[currIndex][i] == lastAccessedQs[currIndex].front())
                    {
                        lastAccessedQs[currIndex].pop_front();
                        tag[currIndex][i] = currTag;
                        validBit[currIndex][i] = true;
                        break;
                    }
                }
            }
        }

        // lru entry has been removed. add current entry to back as mru
        lastAccessedQs[currIndex].push_back(currTag);
    }

    void writeReq(UINT32 virtualAddr)
    {
        // increment counter
        writeReqs++;

        // get index and tag
        UINT32 currIndex = (virtualAddr & indexBitMask) >> logBlockSize;
        UINT32 currTag = virtualAddr >> (logNumRows+logBlockSize);

        // check for write hit
        BOOL didHit = false;
        for (UINT32 i = 0; i < associativity; ++i)
        {
            if (tag[currIndex][i] == currTag)
            {
                // remove current tag from its now stale position in queue (later set it as mru)
                lastAccessedQs[currIndex].remove(currTag);
                validBit[currIndex][i] = true;
                didHit = true;
                writeHits++;
                break;
            }
        }

        // write back and write allocate
        if (!didHit)
        {
            // if queue size is less than associativity, it is empty spot (cold cache)
            if (lastAccessedQs[currIndex].size() < associativity)
            {
                tag[currIndex][ lastAccessedQs[currIndex].size() ] = currTag;
                validBit[currIndex][ lastAccessedQs[currIndex].size() ] = true;
            }
            else
            {
                // check which entry was lru entry at currIndex and replace it
                for (UINT32 i = 0; i < associativity; ++i)
                {
                    if (tag[currIndex][i] == lastAccessedQs[currIndex].front())
                    {
                        lastAccessedQs[currIndex].pop_front();
                        tag[currIndex][i] = currTag;
                        validBit[currIndex][i] = true;
                        break;
                    }
                }
            }
        }

        // lru entry has been removed. add current entry to back as mru
        lastAccessedQs[currIndex].push_back(currTag);
    }
};

//Cache analysis routine
void cacheLoad(UINT32 virtualAddr)
{
    //Here the virtual address is aligned to a word boundary
    virtualAddr = (virtualAddr >> 2) << 2;
    cachePP->readReq(virtualAddr);
    cacheVP->readReq(virtualAddr);
    cacheVV->readReq(virtualAddr);
}

//Cache analysis routine
void cacheStore(UINT32 virtualAddr)
{
    //Here the virtual address is aligned to a word boundary
    virtualAddr = (virtualAddr >> 2) << 2;
    cachePP->writeReq(virtualAddr);
    cacheVP->writeReq(virtualAddr);
    cacheVV->writeReq(virtualAddr);
}

// This knob will set the outfile name
KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE, "pintool",
			    "o", "results.out", "specify optional output file name");

// This knob will set the param logPhysicalMemSize
KNOB<UINT32> KnobLogPhysicalMemSize(KNOB_MODE_WRITEONCE, "pintool",
                "m", "16", "specify the log of physical memory size in bytes");

// This knob will set the param logPageSize
KNOB<UINT32> KnobLogPageSize(KNOB_MODE_WRITEONCE, "pintool",
                "p", "12", "specify the log of page size in bytes");

// This knob will set the cache param logNumRows
KNOB<UINT32> KnobLogNumRows(KNOB_MODE_WRITEONCE, "pintool",
                "r", "10", "specify the log of number of rows in the cache");

// This knob will set the cache param logBlockSize
KNOB<UINT32> KnobLogBlockSize(KNOB_MODE_WRITEONCE, "pintool",
                "b", "5", "specify the log of block size of the cache in bytes");

// This knob will set the cache param associativity
KNOB<UINT32> KnobAssociativity(KNOB_MODE_WRITEONCE, "pintool",
                "a", "2", "specify the associativity of the cache");

// Pin calls this function every time a new instruction is encountered
VOID Instruction(INS ins, VOID *v)
{
    if(INS_IsMemoryRead(ins))
        INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)cacheLoad, IARG_MEMORYREAD_EA, IARG_END);
    if(INS_IsMemoryWrite(ins))
        INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)cacheStore, IARG_MEMORYWRITE_EA, IARG_END);
}

// This function is called when the application exits
VOID Fini(INT32 code, VOID *v)
{
    ofstream outfile;
    outfile.open(KnobOutputFile.Value().c_str());
    outfile.setf(ios::showbase);
    outfile << "physical index physical tag: ";
    cachePP->dumpResults(&outfile);
     outfile << "virtual index physical tag: ";
    cacheVP->dumpResults(&outfile);
     outfile << "virtual index virtual tag: ";
    cacheVV->dumpResults(&outfile);
    outfile.close();
}

// argc, argv are the entire command line, including pin -t <toolname> -- ...
int main(int argc, char * argv[])
{
    // Initialize pin
    PIN_Init(argc, argv);

    logPageSize = KnobLogPageSize.Value();
    logPhysicalMemSize = KnobLogPhysicalMemSize.Value();

    cachePP = new LruPhysIndexPhysTagCacheModel(KnobLogNumRows.Value(), KnobLogBlockSize.Value(), KnobAssociativity.Value());
    cacheVP = new LruVirIndexPhysTagCacheModel(KnobLogNumRows.Value(), KnobLogBlockSize.Value(), KnobAssociativity.Value());
    cacheVV = new LruVirIndexVirTagCacheModel(KnobLogNumRows.Value(), KnobLogBlockSize.Value(), KnobAssociativity.Value());

    // Register Instruction to be called to instrument instructions
    INS_AddInstrumentFunction(Instruction, 0);

    // Register Fini to be called when the application exits
    PIN_AddFiniFunction(Fini, 0);

    // Start the program, never returns
    PIN_StartProgram();

    return 0;
}
