"""
Script to parse results into a csv for pretty-fying the data processing
(and to NOT needing to copy paste numbers a gazillion times)

Creates the following csv's:
1. detailed_data - details of all tests for all configurations
2. config_data   - average data for each cache configuration (avg taken across all models)
3. pipt_avg_data - average data for each cache configuration for pipt model
3. vivt_avg_data - average data for each cache configuration for vivt model
3. vipt_avg_data - average data for each cache configuration for vipt model

"""

import numpy as np
import os
import pandas as pd

# detailed data for each test for each config
detailed_df = pd.DataFrame(columns=("config(rows,bs,as)", "testname", "type", "readreqs", "writereqs", "readhits", "writehits", "readhitrate", "writehitrate"))

# avg (of all tests) data for a particular config
config_df = pd.DataFrame(columns=("config(rows,bs,as)", "avg read hitrate", "avg write hitrate"))

# each directory has results for a particular config. name of dir is the cache config
# data_dirs = ['921', '922', '923', '924']
data_dirs = [ d for d in os.listdir() if (os.path.isdir(d) and not d.startswith(".")) ]
det_row_idx = 0
config_row_idx = 0

for curr_dir in data_dirs:
	
	# add details of each test to df
	rhr_list = []
	whr_list = []
	out_files = [ ofile for ofile in os.listdir(curr_dir) if ofile.endswith(".out")]
	for ofile in out_files:
		with open(curr_dir+"/"+ofile, 'r') as f_handler:
			for line in f_handler.readlines():
				ctype, data = line.split(":")
				rr, wr, rh, wh = map(int, (data.split(",")))
				rhr = rh/rr
				whr = wh/wr
				rhr_list.append(rhr)
				whr_list.append(whr)
				detailed_df.loc[det_row_idx] = [curr_dir, ofile.split("___")[0], ctype, rr, wr, rh, wh, rhr, whr]
				det_row_idx += 1
	
	# add avg vals for graphing to config df
	config_df.loc[config_row_idx] = [curr_dir, np.mean(rhr_list), np.mean(whr_list)]
	config_row_idx += 1

# save csv files
detailed_df.to_csv("detailed_data.csv")
config_df.to_csv("config_data.csv")

# get data corresponding to each model
pipt_df = detailed_df[ detailed_df["type"]=="physical index physical tag"]
vivt_df = detailed_df[ detailed_df["type"]=="virtual index virtual tag"]
vipt_df = detailed_df[ detailed_df["type"]=="virtual index physical tag"]

# save avg data for each config for each model
pipt_df.groupby("config(rows,bs,as)").mean().to_csv("pipt_avg_data.csv")
vivt_df.groupby("config(rows,bs,as)").mean().to_csv("vivt_avg_data.csv")
vipt_df.groupby("config(rows,bs,as)").mean().to_csv("vipt_avg_data.csv")
